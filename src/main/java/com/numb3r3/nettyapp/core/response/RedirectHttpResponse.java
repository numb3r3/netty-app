package com.numb3r3.nettyapp.core.response;


import com.numb3r3.nettyapp.core.response.watcher.Info;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpResponseStatus;
import io.netty.handler.codec.http.HttpVersion;
import io.netty.handler.codec.http.QueryStringDecoder;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;

import java.util.List;

/**
 *
 * @author Roman
 */
class RedirectHttpResponse {

    public FullHttpResponse getResponse(QueryStringDecoder qsd) {
        List<String> url = qsd.parameters().get("url");
        Info.redirectCount(url.get(0));
        FullHttpResponse response = new DefaultFullHttpResponse(HttpVersion.HTTP_1_1, HttpResponseStatus.MOVED_PERMANENTLY);
        response.headers().set(LOCATION, url.get(0));
        return response;
    }
}