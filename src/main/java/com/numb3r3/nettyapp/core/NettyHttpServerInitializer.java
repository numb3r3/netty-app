package com.numb3r3.nettyapp.core;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpRequestDecoder;
import io.netty.handler.codec.http.HttpResponseEncoder;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.traffic.GlobalTrafficShapingHandler;
import io.netty.handler.traffic.TrafficCounter;

/**
 * @author Roman
 */
public class NettyHttpServerInitializer extends ChannelInitializer<SocketChannel> {
//    private TrafficCounter trafficCounter;
    @Override
    public void initChannel(SocketChannel sc) throws Exception {

        /*
        * Take IP below
         */
        String ip = sc.remoteAddress().getHostString();


        ChannelPipeline p = sc.pipeline();

//        GlobalTrafficShapingHandler globalTrafficShapingHandler = new GlobalTrafficShapingHandler(sc.eventLoop());
//        trafficCounter = globalTrafficShapingHandler.trafficCounter();
//
//        p.addLast("traffic", globalTrafficShapingHandler);

        p.addLast("codec", new HttpServerCodec());
//        p.addLast("decoder", new HttpRequestDecoder());
//        p.addLast("encoder", new HttpResponseEncoder());
//        p.addLast("aggregator", new HttpObjectAggregator(65536));
        p.addLast("handler", new NettyHttpServerHandler());
    }
}